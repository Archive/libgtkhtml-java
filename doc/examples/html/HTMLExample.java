package html;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.gnu.gtk.Button;
import org.gnu.gtk.CellRenderer;
import org.gnu.gtk.CellRendererText;
import org.gnu.gtk.DataColumn;
import org.gnu.gtk.DataColumnString;
import org.gnu.gtk.Entry;
import org.gnu.gtk.Frame;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.HBox;
import org.gnu.gtk.HPaned;
import org.gnu.gtk.Label;
import org.gnu.gtk.ScrolledWindow;
import org.gnu.gtk.SelectionMode;
import org.gnu.gtk.ShadowType;
import org.gnu.gtk.TreeIter;
import org.gnu.gtk.TreeSelection;
import org.gnu.gtk.TreeStore;
import org.gnu.gtk.TreeView;
import org.gnu.gtk.TreeViewColumn;
import org.gnu.gtk.VBox;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.TreeSelectionEvent;
import org.gnu.gtk.event.TreeSelectionListener;
import org.gnu.gtkhtml.HTMLDocument;
import org.gnu.gtkhtml.HTMLView;
import org.gnu.gtkhtml.event.HTMLDocumentEvent;
import org.gnu.gtkhtml.event.HTMLDocumentListener;

public class HTMLExample {
	private HTMLDocument document;
	private HTMLView view;
	private TreeStore model;
	private TreeView treeView;
	private Entry entry;
	private DataColumnString dataString1 = new DataColumnString();
	private DataColumnString dataString2 = new DataColumnString();

	private String documents[][] = { 
				{ "Getting Started", "html/c28.html" }, 
				{"Adding Menus", "html/c102.html" }, 
				{"Adding Menus (continued)", "html/x133.html" }, 
				{"ToolBars and StatusBars", "html/c278.html" }, 
				{"StatusBars", "html/x297.html" }, 
				{"Layout Management", "html/c312.html" }, 
				{"Tables", "html/x438.html" }, 
				{"Using Layout Managers", "html/x525.html" },
				{"Java-GNOME Homepage", "http://java-gnome.sourceforge.net"}
	};

	public HTMLExample() {
		// create the main window
		Window window = new Window(WindowType.TOPLEVEL);
		window.setDefaultSize(800, 600);
		window.addListener(new LifeCycleListener() {
			public void lifeCycleEvent(LifeCycleEvent event) {}
			public boolean lifeCycleQuery(LifeCycleEvent event) {
				if (event.isOfType(LifeCycleEvent.Type.DESTROY) || 
						event.isOfType(LifeCycleEvent.Type.DELETE)) {
						Gtk.mainQuit();
				}
				return false;
			}
		});

		// create the document and view
		document = new HTMLDocument();
		document.addListener(new HTMLDocumentListener() {
			public void documentEvent(HTMLDocumentEvent event) {
				if (event.isOfType(HTMLDocumentEvent.Type.LINK_CLICKED))
					System.out.println("link clicked: " + event.getURL());
			}
		});
		view = new HTMLView();

		// create a ScrolledWindow to hold the HTMLView object
		ScrolledWindow sw = new ScrolledWindow();
		sw.add(view);

		// vertical box to hold the controls.
		VBox vbox = new VBox(false, 0);
		
		// create a couple of controls that allows users to enter
		// urls and see them displayed in the html view.
		HBox hbox = new HBox(false, 0);
		Label label = new Label("_url:", true);
		hbox.packStart(label, false, false, 10);
		
		entry = new Entry();
		entry.setText("http://");
		hbox.packStart(entry, true, true, 10);
		
		Button button = new Button("Find");
		button.addListener(new ButtonListener() {
			public void buttonEvent(ButtonEvent event) {
				if (event.isOfType(ButtonEvent.Type.CLICK)) {
					loadFile(entry.getText());
				}
			}
		});
		hbox.packStart(button, false, false, 10);
		vbox.packStart(hbox, false, false, 5);
		
		// create the hpanned object to hold the tree and html view
		HPaned hpanned = new HPaned();
		treeView = createTree();

		Frame frame = new Frame();
		frame.setShadow(ShadowType.IN);
		frame.add(treeView);
		hpanned.add1(frame);

		frame = new Frame();
		frame.setShadow(ShadowType.IN);
		frame.add(sw);
		hpanned.add2(frame);

		vbox.packStart(hpanned, true, true, 0);
		window.add(vbox);

		view.setDocument(document);

		window.showAll();
	}

	private TreeView createTree() {
		model = new TreeStore(new DataColumn[] { dataString1, dataString2 });
		treeView = new TreeView(model);
		TreeSelection selection = treeView.getSelection();
		selection.setMode(SelectionMode.SINGLE);
		selection.addListener(new TreeSelectionListener() {
			public void selectionChangedEvent(TreeSelectionEvent event) {
				TreeSelection mySelection = (TreeSelection) event.getSource();
				TreeIter anIter = model.getFirstIter();
				while (null != anIter) {
					if (mySelection.getSelected(anIter)) {
						String page = model.getValue(anIter, dataString2);
						loadFile(page);
						break;
					}
					anIter = anIter.getNextIter();
				}
			}
		});

		for (int i = 0; i < documents.length; i++) {
			TreeIter iter = model.appendRow(null);
			model.setValue(iter, dataString1, documents[i][0]);
			model.setValue(iter, dataString2, documents[i][1]);
		}

		CellRenderer renderer = new CellRendererText();
		TreeViewColumn column = new TreeViewColumn();
		column.setTitle("Documents");
		column.packStart(renderer, true);
		column.addAttributeMapping(renderer, CellRendererText.Attribute.TEXT, dataString1);
		treeView.appendColumn(column);

		return treeView;
	}

	private void loadFile(String filename) {
		try {
			view.setDocument(null);
			if (filename.indexOf("http") >= 0)
				document.loadURL(filename);
			else
				document.loadFile(filename);
			view.setDocument(document);
		} catch (FileNotFoundException e1) {
			System.out.println("File not found " + filename);
			System.out.println(e1);
		} catch (MalformedURLException e3) {
			System.out.println("Error reading url " + filename);
			System.out.println(e3);
		} catch (IOException e2) {
			System.out.println("Error reading file " + filename);
			System.out.println(e2);
		}
	}

	public static void main(String[] args) {
		Gtk.init(args);
		HTMLExample html = new HTMLExample();
		Gtk.main();
	}
}
