package TestHTML;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import org.gnu.gdk.ModifierType;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.Label;
import org.gnu.gtk.Entry;
import org.gnu.gtk.HBox;
import org.gnu.gtk.VBox;
import org.gnu.gtk.ScrolledWindow;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.HSeparator;
import org.gnu.gtk.PolicyType;
import org.gnu.gtk.event.EntryEvent;
import org.gnu.gtk.event.EntryListener;
import org.gnu.gtk.event.KeyEvent;
import org.gnu.gtk.event.KeyListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtkhtml.HTMLView;
import org.gnu.gtkhtml.HTMLDocument;
import org.gnu.gtkhtml.HTMLStream;
import org.gnu.gtkhtml.dom.DomMouseEvent;
import org.gnu.gtkhtml.dom.DomMouseListener;
import org.gnu.gtkhtml.event.HTMLDocumentEvent;
import org.gnu.gtkhtml.event.HTMLDocumentListener;
import org.gnu.gtkhtml.event.HTMLViewEvent;
import org.gnu.gtkhtml.event.HTMLViewListener;

/**
 * Class to test the features of the libgtkhtml-java library.
 *
 * <p>
 * TODO:
 *   - Change the location entry (ent) to show the current page.
 *   - Add a status bar to show the links on the ON_URL event.
 */
public class TestHTML {

    private static final String DEFAULT_PAGE = 
        "http://www.google.com/";

    private Window window;
    private HTMLView view;
    private Entry ent;
    private String base;

    public static void main( String[] argv ) {
        Gtk.init( argv );
        TestHTML th = new TestHTML();
        Gtk.main();
    }

    public TestHTML() {
        window = new Window( WindowType.TOPLEVEL );
	window.setTitle( "Test HTML" );
	window.addListener( new Life() );

        VBox vbox = new VBox( false, 0 );

        HBox hbox = new HBox( false, 5 );
        hbox.setBorderWidth( 10 );
        Label lbl = new Label( "Location:" );
        ent = new Entry();
        ent.addListener( new LocationListener() );
        ent.setText( "http://www.kernel.org" );
        hbox.packStart( lbl, false, false, 0 );
        hbox.packStart( ent );
        vbox.packStart( hbox, false, false, 0 );

        HSeparator sep = new HSeparator();
        vbox.packStart( sep, false, false, 0 );

        ScrolledWindow sw = new ScrolledWindow();
        sw.setPolicy( PolicyType.AUTOMATIC, PolicyType.AUTOMATIC );
        view = new HTMLView();
        view.addListener( new BrowserListener() );
        sw.add( view );
        vbox.packStart( sw );

        loadDocument( DEFAULT_PAGE );

	window.add( vbox );
        window.addListener( new Keys() );
        window.resize( 300, 200 );
        window.showAll();
    }

    /**
     * Load the HTML document from the given URL location.
     */
    private void loadDocument( String url ) {
        base = url;
        HTMLDocument document = new HTMLDocument();
        BrowserListener listen = new BrowserListener();
        document.addListener( (HTMLDocumentListener)listen );

        // Uncomment this line if you want to see what events are available
        // with the DomMouseListener.
        //document.addListener( (DomMouseListener)listen );

        try {
            document.loadURL( url );
            view.setDocument( document );
        } catch( IOException ex ) {
            System.err.println( "TestHTML#loadDocument[IOE]( " + url + 
                                " ) - " + ex );
        }

    }

    /**
     * Load an external source into the current document.  This will load 
     * images and CSS files.  It is called from the HTMLDocumentListener 
     * instance when a REQUEST_URL notification is received.
     */
    private void loadInternal( String url, HTMLStream stream ) {
        try {
            URL surl = new URL( url );
            stream.loadStream( surl.openStream() );
        } catch( IOException ex ) {
            System.err.println( "TestHTML#loadInternal[IOE]( " + url + 
                                " ) - " + ex );
        }
    }

    /**
     * Get the full URL to retrieve based on the given base URL and 
     * the requested URL.
     */ 
    private static String getFullURL( String base, String requested_url ) {
        // Requested URL is self supporting.
        if ( requested_url.startsWith( "http://" )  ||
             requested_url.startsWith( "https://" ) ||
             requested_url.startsWith( "file://" )   ) {
            return requested_url;
        }
        // Do we have a valid base URL.
        if ( base == null || base.trim().equals( "" ) ||
             ( !base.startsWith( "http://" ) &&
               !base.startsWith( "file://" ) &&
               !base.startsWith( "https://" ) ) ) {
            // No valid base URL specified, just return the requested URL!
            return requested_url;
        } else {
            // Base starts with http://, file:// or https://

            if ( requested_url.startsWith( "/" ) ) {
                // Absolute path.
                base = getDomainWithProtocol( base );
            } else {
                // Relative path.
                base = getURLWithoutFile( base );
            }
            return concatURLParts( base, requested_url );
        }
    }

    private static String getURLWithoutFile( String url ) {
        int i = url.lastIndexOf( '/' );
        if ( i < 7 ) {
            return url;
        } else {
            return url.substring( 0, i + 1 );
        }
    }

    private static String getDomainWithProtocol( String url ) {
        if ( url.startsWith( "http://" ) ||
             url.startsWith( "file://" ) ||
             url.startsWith( "https://" ) ){
            int i = url.indexOf( '/', 8 );
            if ( i == -1 ) {
                return url;
            } else {
                return url.substring( 0, i );
            }
        } else {
            return url;
        }
    }

    /**
     * Combine the given URL parts into a complete URL String, removing any
     * extra "/"s that might arrise from a normal concatenation.
     */
    private static String concatURLParts( String first, String second ) {
        if ( first.endsWith( "/" ) && second.startsWith( "/" ) ) {
            return first + second.substring( 1 );
        }
        if ( !first.endsWith( "/" ) && !second.startsWith( "/" ) ) {
            return first + '/' + second;
        }
        return first + second;
    }

    /**
     * Listener for events on the URL entry widget.
     */
    protected class LocationListener implements EntryListener {
        public void entryEvent( EntryEvent event ) {
            if ( event.isOfType( EntryEvent.Type.ACTIVATE ) ) {
                String loc = ent.getText();
                System.out.println( "activated! - " + loc );
                loadDocument( loc );
            }
        }
    }

    /**
     * Listener for events on the HTMLDocument and HTMLView widgets.
     */
    protected class BrowserListener 
        implements HTMLDocumentListener, DomMouseListener, HTMLViewListener {

        public void viewEvent( HTMLViewEvent event ) {
            System.out.println( "HTMLViewEvent: " + event.getType().getName() +
                                ";" + event.getURL() );
        }

        public void domMouseEvent( DomMouseEvent event ) {
            System.out.println( "DomMouseEvent: " + 
                                event.getType().getName() );
        }
                                
        public void documentEvent( HTMLDocumentEvent event ) {
            if ( event.isOfType( HTMLDocumentEvent.Type.REQUEST_URL ) ) {

                String url = getFullURL( base, event.getURL() );
                System.out.println( "HTMLDocumentEvent: loading - " + url );
                loadInternal( url, event.getHtmlStream() );

            } else if ( event.isOfType( HTMLDocumentEvent.Type.LINK_CLICKED ) ) {
                String url = getFullURL( base, event.getURL() );
                System.out.println( "HTMLDocumentEvent: clicked - " + url );
                loadDocument( url );
                
            } else if ( event.isOfType( HTMLDocumentEvent.Type.SET_BASE ) ) {

                base = event.getURL();

            } else if ( event.isOfType( HTMLDocumentEvent.Type.TITLE_CHANGED ) ) {
                window.setTitle( event.getURL() );

            } else if ( event.isOfType( HTMLDocumentEvent.Type.SUBMIT ) ) {

                String url = getFullURL( base, event.getURL() );
                System.out.println( "HTMLDocumentEvent: submit - " + url );
                loadDocument( url );

            } else {

                System.err.println( "  How did we get here!?!" );

            }
        }
    }

    /**
     * Listener for key events on the main window.  Used to catch the Ctrl-C
     * and exit.
     */
    protected class Keys implements KeyListener {
        public boolean keyEvent( KeyEvent event ) {
            // Ctrl-Q quits!
            if ( event.getKeyval() == 113 && 
                 event.getModifierKey() == ModifierType.CONTROL_MASK &&
                 event.isOfType( KeyEvent.Type.KEY_RELEASED ) ) {
                Gtk.mainQuit();
            }
            return false;
        }
    }

    /**
     *  Listener for life cycle events on the main window.
     */
    protected class Life implements LifeCycleListener {
        public void lifeCycleEvent(LifeCycleEvent event) {}
        public boolean lifeCycleQuery(LifeCycleEvent event) {
            if (event.isOfType(LifeCycleEvent.Type.DESTROY) || 
                event.isOfType(LifeCycleEvent.Type.DELETE)) {
                Gtk.mainQuit();
            }
            return false;
        }
    }
}
