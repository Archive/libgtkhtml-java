package org.gnu.gtkhtml.event;

import org.gnu.glib.EventType;
import org.gnu.gtk.event.GtkEvent;

/**
 * An event represeting action by a {@link org.gnu.gtkhtml.HTMLView} widget.
 */
public class HTMLViewEvent extends GtkEvent {
	
    private String url = null;

    /**
     * Type of a HtmlViewEvent
     */
    public static class Type extends EventType {
        private Type(int id, String name) {
            super(id, name);
        }
        public static final Type ON_URL = new Type(1, "ON_URL");
    }

    /**
     * Creates a new Button HTMLDocumentEvent. 
     * This is used internally by java-gnome. Users
     * only have to deal with listeners.
     */
    public HTMLViewEvent(Object source, HTMLViewEvent.Type type) {
        super(source, type);
    }

    /**
     * @return True if the type of this event is the same as that stated.
     */
    public boolean isOfType(HTMLViewEvent.Type aType) {
        return (type.getID() == aType.getID());
    }

    public String getURL() {
        return url;
    }

    public void setURL(String u) {
        url = u;
    }
}
