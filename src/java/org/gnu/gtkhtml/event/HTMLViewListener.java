package org.gnu.gtkhtml.event;

/**
 * Listener for the {@link org.gnu.gtkhtml.HTMLView} widget.
 */
public interface HTMLViewListener {

    /**
     * This method is called whenever a html view event occurs.
     */
    public void viewEvent(HTMLViewEvent event );
}
