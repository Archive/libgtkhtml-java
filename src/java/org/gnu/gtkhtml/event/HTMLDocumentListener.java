/*
 * Java-Gnome Bindings Library
 *
 * * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.gtkhtml.event;

/**
 * Listener for the {@link org.gnu.gtkhtml.HTMLDocument} widget.
 */
public interface HTMLDocumentListener {

    /**
     * This method is called whenever a html document event occurs.
     */
    public void documentEvent(HTMLDocumentEvent event );
}
