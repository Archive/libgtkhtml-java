/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtkhtml;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

public class HTMLContext extends GObject {

    public HTMLContext( Handle handle ) {
        super( handle );
    }

    public HTMLContext() {
        super( gtk_html_context_get() );
    }

    native static final protected int gtk_html_context_get_type();
    native static final protected Handle gtk_html_context_get();
}
