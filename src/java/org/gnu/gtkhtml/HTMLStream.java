package org.gnu.gtkhtml;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 * Class that wraps the libgtkhtml C struct, HtmlStream.
 * <p>
 * Normal usage of this class is via an {@link 
 * org.gnu.gtkhtml.event.HTMLDocumentEvent} for loading images
 * and CSS files.
 *
 * @see org.gnu.gtkhtml.events.HTMLDocumentEvent
 */
public class HTMLStream extends GObject {

    public HTMLStream( Handle handle ) {
        super( handle );
    }

    public void loadStream( InputStream stream ) 
        throws IOException {
        try {
            BufferedInputStream in = 
                new BufferedInputStream( stream );
            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = in.read(buffer)) != -1) {
                write( buffer, bytesRead );
            }
        } finally {
            close();
        }
    }

    public void loadURL( String page ) 
        throws MalformedURLException, IOException {
        URL url = new URL( page );
        loadStream( url.openStream() );
    }

    public void loadFile( String filename ) 
        throws IOException {
        File aFile = new File(filename);
        loadStream( new FileInputStream( aFile ) );
    }

    public void setMimeType( String mime ) {
        html_stream_set_mime_type( getHandle(), mime );
    }

    public void write(byte[] buffer, int length) {
        html_stream_write( getHandle(), buffer, length );
    }

    public void close() {
        html_stream_close( getHandle() );
    }

    public void cancel() {
        html_stream_cancel( getHandle() );
    }

    native static final protected void html_stream_write( Handle stream, byte[] buffer, int size );
    native static final protected void html_stream_close( Handle stream );
    native static final protected void html_stream_set_mime_type( Handle stream, String mime_type );
    native static final protected void html_stream_cancel( Handle stream );
}

