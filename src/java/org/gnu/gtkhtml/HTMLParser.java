/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtkhtml;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

public class HTMLParser extends GObject {

    public HTMLParser(HTMLDocument document, HTMLParserType type) {
        super(html_parser_new(document.getHandle(), type.getValue()));
    }
	
    native static final protected int html_parser_get_type();
    native static final protected Handle html_parser_new(Handle document, int parserType);
}
