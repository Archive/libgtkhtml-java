/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtkhtml.dom;

import org.gnu.glib.Handle;
import org.gnu.gtkhtml.dom.DomNode;

public class DomDocumentType extends DomNode {

    /*
     * Constructor for DomDocumentType, given a handle
     * @param hand Handle to assign to this instance
     */
    public DomDocumentType(Handle hand)
    {
	super(hand);
    }

    /*
     * Returns the name of the Document Type
     * @return String the Document Type name
     */
    public String getName() {
	return (dom_DocumentType_get_name(getHandle()));
    }

    /*
     * Returns the public ID of the Document Type
     * @return String the Document Type's Public ID
     */
    public String getPublicId() {
	return (dom_DocumentType_get_publicId(getHandle()));
    }

    /*
     * Returns the system ID of the Document Type
     * @return String the Document Type's System ID
     */
    public String getSystemId() {
	return (dom_DocumentType_get_systemId(getHandle()));
    }

    /*
     * Returns a map of the entities (a DomNamedNodeMap)
     * Currently, this is just an opaque pointer until I wrap DomNamedNodeMap
     * @return int a pointer to the map of entities in C space
     */
    public int getEntities() {
	return (dom_DocumentType_get_entities(getHandle()));
    }

    native static final protected int dom_DocumentType_get_type();
    native static final protected String dom_DocumentType_get_name(Handle dtd);
    native static final protected String dom_DocumentType_get_publicId(Handle dtd);
    native static final protected String dom_DocumentType_get_systemId(Handle dtd);
    native static final protected int dom_DocumentType_get_entities(Handle dtd);
}
