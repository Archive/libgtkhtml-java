/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtkhtml.dom;

import org.gnu.glib.Handle;

/**
 */
public class DomDocument extends DomNode {

    native static final protected int dom_document_get_type();
    native static final protected Handle dom_document_new();
    native static final protected Handle dom_document_get_documentElement(Handle doc);
    native static final protected Handle dom_document_createElement(Handle doc, String tagName);
    native static final protected Handle dom_document_createTextNode(Handle doc, String data);
    native static final protected Handle dom_document_createComment(Handle doc, String data);
    native static final protected Handle dom_document_importNode(Handle doc, int importedNode, boolean deep, int[] exception);
}
