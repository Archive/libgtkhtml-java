/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtkhtml.dom;

import org.gnu.glib.GObject;

/**
 */
public class DomNamedNodeMap extends GObject {

	/****************************************
	 * BEGINNING OF JNI CODE
	 ****************************************/
    native static final protected int dom_namednodemap_getnameditem(int map, String name);
    native static final protected int dom_namednodemap_setnameditem(int map, int arg, int except);
    native static final protected int dom_namednodemap_removenameditem(int map, String name, int except);
    native static final protected int dom_namednodemap_get_item(int map, long index);
    native static final protected long dom_namednodemap_get_length(int map);
	/****************************************
	 * END OF JNI CODE
	 ****************************************/
}
