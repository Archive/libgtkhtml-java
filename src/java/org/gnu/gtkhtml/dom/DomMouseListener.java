package org.gnu.gtkhtml.dom;

/**
 * Listener for DOM mouse events on an {@link org.gnu.gtkhtml.HTMLDocument} 
 * widget.
 */
public interface DomMouseListener {
	/**
	 * This method is called whenever a DOM mouse event occurs in the 
	 * HTMLDocument.
	 */
	public void domMouseEvent(DomMouseEvent event );
}
