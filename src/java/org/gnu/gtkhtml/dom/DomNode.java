/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtkhtml.dom;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

public class DomNode extends GObject {
    // This constructor should not be called, but it necessary for subclasses
    protected DomNode() {
    }

    public DomNode(Handle handl) {
	super(handl);
    }

    public DomDocumentType getDocumentType()
    {
        Handle handl = dom_Node_get_doctype(getHandle());
	return new DomDocumentType(handl);
    }

    public long getType() {
	return (dom_node_get_type());
    }

    public DomNode makeRef() {
	return (new DomNode(getHandle()));
    }

    native static final protected int dom_node_get_type();
    native static final protected int dom_Node_mkref(Handle node);
    native static final protected int dom_Node_get_childNodes(Handle node);
    native static final protected int dom_Node_removeChild(Handle node, Handle oldChild, int[] exception);
    native static final protected String dom_Node_get_nodeValue(Handle node, int[] exception);
    native static final protected int dom_Node_get_firstChild(Handle node);
    native static final protected String dom_Node_get_nodeName(Handle node);
    native static final protected int dom_Node_get_attributes(Handle node);
    native static final protected boolean dom_Node_hasChildNodes(Handle node);
    native static final protected int dom_Node_get_parentNode(Handle node);
    native static final protected int dom_Node_get_nextSibling(Handle node);
    native static final protected int dom_Node_get_nodeType(Handle node);
    native static final protected int dom_Node_cloneNode(Handle node, boolean deep);
    native static final protected int dom_Node_appendChild(Handle node, Handle newChild, int[] exception);
    native static final protected String dom_Node_get_localName(Handle node);
    native static final protected String dom_Node_get_namespaceURI(Handle node);
    native static final protected int dom_Node_get_previoussibling(Handle node);
    native static final protected int dom_Node_get_lastChild(Handle node);
    native static final protected void dom_Node_set_nodeValue(Handle node, String value, int[] exception);
    native static final protected int dom_Node_get_ownerDocument(Handle node);
    native static final protected boolean dom_Node_hasAttributes(Handle node);
    native static final protected Handle dom_Node_get_doctype(Handle node);
}
