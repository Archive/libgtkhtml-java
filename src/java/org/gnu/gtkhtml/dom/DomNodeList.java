/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtkhtml.dom;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;
import org.gnu.gtkhtml.dom.DomNode;

/**
 */
public class DomNodeList extends GObject {
    /*
     * Construct a DomNodeList object given a DomNodeList pointer (from the C side)
     * @param handl Handle of an existing DomNodeList
     */
    public DomNodeList(Handle handl) {
	super(handl);
    }

    /*
     * Returns the type
     * @return the type of a DomNodeList
     */
    public int getType() {
	return (dom_node_list_get_type());
    }

    /*
     * Return the length of a DomNodeList
     * @return the length of the list
     */
    public int getLength() {
	return ((int) dom_node_list_get_length(getHandle()));
    }

    /*
     * Return a specific item from a DomNodeList
     * @param which The index of the item to return
     * @return a DomNode object from the list
     */
    public DomNode getItem(int which) {
	DomNode node = new DomNode(dom_node_list_get_item(getHandle(), which));
	return (node);
    }

    native static final protected int dom_node_list_get_type();
    /* native static final protected int dom_node_list_new(); */
    native static final protected long dom_node_list_get_length(Handle list);
    native static final protected Handle dom_node_list_get_item(Handle list, long index);
}
