/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtkhtml;

import org.gnu.glib.Enum;

public class HTMLParserType extends Enum {

    static final private int _HTML = 0;
    static final public HTMLParserType HTML = new HTMLParserType(_HTML);

    static final private int _XML = 1;
    static final public HTMLParserType XML = new HTMLParserType(_XML);

    static final private HTMLParserType[] theInterned = new HTMLParserType[] { HTML, XML };

    static private java.util.Hashtable theInternedExtras;
    static final private HTMLParserType theSacrificialOne = new HTMLParserType(0);

    static public HTMLParserType intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        HTMLParserType already = (HTMLParserType) theInternedExtras.get(theSacrificialOne);
        if (already == null) {
            already = new HTMLParserType(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private HTMLParserType(int value) {
        value_ = value;
    }

    public HTMLParserType or(HTMLParserType other) {
        return intern(value_ | other.value_);
    }

    public HTMLParserType and(HTMLParserType other) {
        return intern(value_ & other.value_);
    }

    public HTMLParserType xor(HTMLParserType other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(HTMLParserType other) {
        return (value_ & other.value_) == other.value_;
    }

}
