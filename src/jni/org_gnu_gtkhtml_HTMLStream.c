/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
#include <sys/types.h>
#include <libgtkhtml/gtkhtml.h>
#include <libgtk-java/jg_jnu.h>

#ifndef _Included_org_gnu_gtkhtml_HTMLStream
#define _Included_org_gnu_gtkhtml_HTMLStream
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_gtkhtml_HTMLStream
 * Method:    html_stream_write
 * Signature: (ILjava/lang/String;I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtkhtml_HTMLStream_html_1stream_1write
(JNIEnv *env, jclass cls, jobject stm, jbyteArray buf, jint len)
{
    HtmlStream *stream = (HtmlStream*)getPointerFromHandle(env, stm);
    jbyte *buffer = (*env)->GetByteArrayElements(env, buf, NULL );
    html_stream_write(stream, buffer, (gint)len);
    (*env)->ReleaseByteArrayElements(env, buf, buffer, 0);
}

/*
 * Class:     org_gnu_gtkhtml_HTMLStream
 * Method:    html_stream_close
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtkhtml_HTMLStream_html_1stream_1close
(JNIEnv *env, jclass cls, jobject stm)
{
    html_stream_close( (HtmlStream*)getPointerFromHandle(env, stm) );
}

/*
 * Class:     org_gnu_gtkhtml_HTMLStream
 * Method:    html_stream_set_mime_type
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtkhtml_HTMLStream_html_1stream_1set_1mime_1type
(JNIEnv *env, jclass cls, jobject stm, jstring mime)
{
    const gchar *mimeType = NULL;
    if ( mime != NULL )
        mimeType = (gchar*)(*env)->GetStringUTFChars(env, mime, 0);

    html_stream_set_mime_type( (HtmlStream*)getPointerFromHandle(env, stm), 
                               mime );
  
    if ( mime != NULL )
        (*env)->ReleaseStringUTFChars(env, mime, mimeType);
}

/*
 * Class:     org_gnu_gtkhtml_HTMLStream
 * Method:    html_stream_cancel
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_gtkhtml_HTMLStream_html_1stream_1cancel
(JNIEnv *env, jclass cls, jobject stm)
{
    html_stream_cancel( (HtmlStream*)getPointerFromHandle(env, stm) );
}

#ifdef __cplusplus
}
#endif
#endif
